// vim: et ts=4:

#include <stdio.h>
#include <stdlib.h>
#include <yaml.h>


#define VERSION "1.0.0"


int usage(int ret) {
    fprintf(stderr, "Usage: yx [<options>] [<path>]\n");
    fprintf(stderr, "<options> :-\n");
    fprintf(stderr, "  -h|--help         : show this help\n");
    fprintf(stderr, "  -d|--doc <n>      : parse <n>th YAML document, default: 1\n");
    fprintf(stderr, "  -f|--file <file>  : parse YAML from <file>, default: STDIN\n");
    fprintf(stderr, "  -v|--version      : display version\n");
    fprintf(stderr, "  --                : end of command options\n");
    fprintf(stderr, "<path> :-\n");
    fprintf(stderr, "  <key>|<index> ... : path to the data to extract\n");
    fprintf(stderr, "<key>               : map key value\n");
    fprintf(stderr, "<index>             : sequence <index> number, starts with 1\n");
    return ret;
}


int get_event(yaml_parser_t *parser, yaml_event_t *event);
int parse_mapping(yaml_parser_t *parser, int c, char *k[]);
int parse_sequence(yaml_parser_t *parser, int c, char *k[]);
int parse_scalar(yaml_event_t *event, int c, char *k[]);


int main(int argc, char *argv[])
{
    FILE *input = stdin;
    int doc = 1;
    int i = 0;

    /* parse command line options */

    for (i = 1; i < argc; i++) {
        if (!strncmp("-h", argv[i], 3) || !strncmp("--help", argv[i], 7)) {
            fprintf(stderr, "YAML Data Extraction Tool\n");
            return usage(0);
        }
        else if (!strncmp("-v", argv[i], 3) || !strncmp("--version", argv[i], 10)) {
            printf("yx v%s\n", VERSION);
            return 0;
        }
        else if (!strncmp("-d", argv[i], 3) || !strncmp("--doc", argv[i], 6)) {
            /* --doc[ument] D */
            if (++i == argc) {
                fprintf(stderr, "ERROR: --doc requires positive integer value\n", argv[i]);
                return usage(1);
            }
            doc = atoi(argv[i]);
            if (doc < 1) {
                fprintf(stderr, "ERROR: '%s' must be an positive integer\n", argv[i]);
                return usage(1);
            }
        }
        else if (!strncmp("-f", argv[i], 3) || !strncmp("--file", argv[i], 7)) {
            /* --file F */
            if (++i == argc) {
                fprintf(stderr, "ERROR: --file requires string value\n", argv[i]);
                return usage(1);
            }
            if (!strncmp("-", argv[i], 2)) continue;    /* STDIN alias */
            input = fopen(argv[i], "rb");
            if (input == NULL) {
                fprintf(stderr, "ERROR: unable to open file '%s'\n", argv[i]);
                return usage(1);
            }
        }
        else if (!strncmp("--", argv[i], 3)) {
            /* stop looking for options */
            i++;
            break;
        }
        else if (!strncmp(argv[i], "-", 1)) {
            /* unknown option */
            fprintf(stderr, "ERROR: unknown option '%s'\n", argv[i]);
            return usage(1);
        } else break;   /* first key argument found */
    }

    // Initialize YAML Parser

    int ret = 0;
    yaml_parser_t parser;
    yaml_event_t event;

    if (!yaml_parser_initialize(&parser)) {
        fprintf(stderr, "ERROR: could not initialize YAML parser\n");
        return 1;
    }
    yaml_parser_set_input_file(&parser, input);

    // Main Parsing Loop

    while (1) {
        yaml_event_type_t type;

        if (!get_event(&parser, &event)) {
            ret = 1;
            break;
        }
        type = event.type;

        if (type == YAML_DOCUMENT_START_EVENT)
            doc--;
        else if (!doc) {
            if (type == YAML_MAPPING_START_EVENT) {
                yaml_event_delete(&event);
                ret = parse_mapping(&parser, argc - i, &argv[i]);
                break;
            }
            else if (type == YAML_SEQUENCE_START_EVENT) {
                yaml_event_delete(&event);
                ret = parse_sequence(&parser, argc - i, &argv[i]);
                break;
            }
            else if (type == YAML_SCALAR_EVENT) {
                ret = parse_scalar(&event, argc - i, &argv[i]);
                break;
            }
            else if (type == YAML_DOCUMENT_END_EVENT) {
                fprintf(stderr, "ERROR: key-path not found in YAML\n");
                ret = 1;
                break;
            }
        }
        else if (type == YAML_STREAM_END_EVENT)
            break;

        yaml_event_delete(&event);
    }
    if (!ret && doc) {
        fprintf(stderr, "ERROR: requested document not found\n");
        ret = 1;
    }

    /* cleanup */
    fclose(input);
    yaml_parser_delete(&parser);
    fflush(stdout);

    return ret;
}


int get_event(yaml_parser_t *parser, yaml_event_t *event) {
    if (!yaml_parser_parse(parser, event)) {
        if (parser->problem_mark.line || parser->problem_mark.column) {
            fprintf(stderr, "ERROR: YAML parsing (%lu,%lu): %s\n",
                (unsigned long)parser->problem_mark.line + 1,
                (unsigned long)parser->problem_mark.column + 1,
                parser->problem);
            }
        else 
            fprintf(stderr, "ERROR: YAML parsing: %s\n", parser->problem);
        return 0;
    }
    return 1;
}


int parse_mapping(yaml_parser_t *parser, int c, char *k[]) {
    yaml_event_t event;
    yaml_event_type_t type;
    int depth = 0;
    char *key = NULL;
    
    if (!get_event(parser, &event))
        return 1;
    type = event.type;

    while (depth || type != YAML_MAPPING_END_EVENT) {
        // sort out key
        if (!depth) {
            if (!key) {
                if (type == YAML_SCALAR_EVENT) {
                    key = malloc(event.data.scalar.length);
                    strncpy(key, event.data.scalar.value, event.data.scalar.length);
                    yaml_event_delete(&event);
                    if (!get_event(parser, &event))
                        return 1;
                    type = event.type;
                }
                else if (type == YAML_MAPPING_START_EVENT || type == YAML_SEQUENCE_START_EVENT) {
                    fprintf(stderr, "ERROR: unsupported non-scalar map key detected\n");
                    return 1;
                }
            }
        }
        if (!c) {
            // no more keys, but we're not at a scalar, output keys
            switch (type) {
                case YAML_MAPPING_START_EVENT:
                case YAML_SEQUENCE_START_EVENT:
                case YAML_SCALAR_EVENT:
                    if (!depth) {
                        printf("%s\n", key);
                        free(key);
                        key = NULL;
                    }
                    if (type != YAML_SCALAR_EVENT)
                        depth++;
                    break;
                case YAML_MAPPING_END_EVENT:
                case YAML_SEQUENCE_END_EVENT:
                    depth--;
            }
        }
        else {    
            switch (type) {
                case YAML_MAPPING_START_EVENT:
                case YAML_SEQUENCE_START_EVENT:
                case YAML_SCALAR_EVENT:
                    if (!depth) {
                        if (!strncmp(k[0], key, strlen(k[0]) + 1)) {
                            // this is the key we're looking for
                            free(key);
                            key = NULL;
                            if (type == YAML_MAPPING_START_EVENT) {
                                yaml_event_delete(&event);
                                return parse_mapping(parser, c - 1, &k[1]);
                            }
                            else if (type == YAML_SEQUENCE_START_EVENT) {
                                yaml_event_delete(&event);
                                return parse_sequence(parser, c - 1, &k[1]);
                            } else {
                                return parse_scalar(&event, c - 1, &k[1]);
                            }
                        }
                        else {
                            free(key);
                            key = NULL;
                        }
                    }
                    if (type != YAML_SCALAR_EVENT)
                        depth++;
                    break;
               case YAML_MAPPING_END_EVENT:
               case YAML_SEQUENCE_END_EVENT:
                    depth--;
            }
        }
        yaml_event_delete(&event);
        if (!get_event(parser, &event))
            return 1;
        type = event.type;
    }
    if (c) {
        fprintf(stderr, "ERROR: unable to find map key '%s'\n", k[0]);
        return 1;
    }
    return 0;
}


int parse_sequence(yaml_parser_t *parser, int c, char *k[]) {
    yaml_event_t event;
    yaml_event_type_t type;
    int depth = 0;
    int n = 0;
    
    if (c) {
        n = atoi(k[0]);
        if (!n) {
            fprintf(stderr, "ERROR: sequence key '%s' not positive integer\n", k[0]);
            return 1;
        }
    }
    if (!get_event(parser, &event))
        return 1;
    type = event.type;

    while (depth || type != YAML_SEQUENCE_END_EVENT) {
        if (!c) {
            // no more keys, but we're not at a scalar, output indexes
            switch (type) {
                case YAML_MAPPING_START_EVENT:
                case YAML_SEQUENCE_START_EVENT:
                case YAML_SCALAR_EVENT:
                    if (!depth)
                        printf("%d\n", ++n);
                    if (type != YAML_SCALAR_EVENT)
                        depth++;
                    break;
                case YAML_MAPPING_END_EVENT:
                case YAML_SEQUENCE_END_EVENT:
                    depth--;
            }
        }
        else {    
            switch (type) {
                case YAML_MAPPING_START_EVENT:
                case YAML_SEQUENCE_START_EVENT:
                case YAML_SCALAR_EVENT:
                    if (!depth) {
                        if (!--n) {
                            // this is the index we're looking for
                            if (type == YAML_MAPPING_START_EVENT) {
                                yaml_event_delete(&event);
                                return parse_mapping(parser, c - 1, &k[1]);
                            }
                            else if (type == YAML_SEQUENCE_START_EVENT) {
                                yaml_event_delete(&event);
                                return parse_sequence(parser, c - 1, &k[1]);
                            } else {
                                return parse_scalar(&event, c - 1, &k[1]);
                            }
                        }
                    }
                    if (type != YAML_SCALAR_EVENT)
                        depth++;
                    break;
               case YAML_MAPPING_END_EVENT:
               case YAML_SEQUENCE_END_EVENT:
                    depth--;
            }
        }
        yaml_event_delete(&event);
        if (!get_event(parser, &event))
            return 1;
        type = event.type;
    }
    if (c) {
        fprintf(stderr, "ERROR: unable to find sequence key '%s'\n", k[0]);
        return 1;
    }
    return 0;
}


int parse_scalar(yaml_event_t *event, int c, char *k[]) {
    if (c != 0) {
        fprintf(stderr, "ERROR: scalar found where key '%s' expected\n", k[0]);
        return 1;
    }
    // TODO: we may want to optionally base64 encode some data?
    // value may contain NUL chars, output each char separately
    for (int i = 0; i < event->data.scalar.length; i++) {
        putchar(event->data.scalar.value[i]);
    }
    return 0;
}

