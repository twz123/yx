# vim: noet ts=8:

PREFIX?=/usr

.PHONY: test clean

yx: yx.c
	cc -Os -o yx yx.c -lyaml
	chmod +x yx

test: yx
	(cd tests && ./do_tests.sh)

install: yx
	install -sDm0755 -t "$(PREFIX)"/bin yx

clean:
	rm yx
