#!/bin/sh
# vim: et ts=4:

VERBOSE=${VERBOSE:-0}
[ "$1" = "-v" ] && VERBOSE=1

TESTS=0
FAILS=0

assert() {
    local testing="$1"
    local execute="$2"
    local should="$3"
    local expected="$4"
    local result
    local retval

    let TESTS=TESTS+1
    printf "--- $testing..."
    result="$($execute 2>&1)"
    retval=$?

    if [ $retval -ne 0 ]; then
        if [ "$should" = "error" ]; then
            printf "error_ok ($retval)\n"
        else
            let FAILS=FAILS+1
            printf "ERROR ($retval)\n"
        fi
    elif [ "$result" != "$expected" ]; then
        if [ "$should" = "fail" ]; then
            printf "fail_ok\n"
        else
            let FAILS=FAILS+1
            printf "FAIL\n"
        fi
    else
        if [ "$should" = "succeed" ]; then
            printf "success\n"
        else
            let FAILS=FAILS+1
            printf "SUCCESS_!OK\n"
        fi
    fi
    [ $VERBOSE -eq 1 ] && printf "$result\n\n"
}

printf "\n=== INPUT ===\n\n"

echo stdin |
assert "From Implicit STDIN" \
    "../yx" \
    "succeed" \
    "stdin"

echo stdin |
assert "From Explicit STDIN" \
    "../yx -f -" \
    "succeed" \
    "stdin"

assert "From Non-Existent File" \
    "../yx -f does-not-exist.yaml" \
    "error"

assert "From Invalid YAML File" \
    "../yx -f ../yx" \
    "error"

printf "\n=== DOCUMENT SELECTION ===\n\n"

assert "Implicit Document 1" \
    "../yx -f test__documents.yaml" \
    "succeed" \
    "one"

assert "Explicit Document 1" \
    "../yx -d 1 -f test__documents.yaml" \
    "succeed" \
    "one"

assert "Explicit Document 2" \
    "../yx -d 2 -f test__documents.yaml" \
    "succeed" \
    "two"

assert "Invalid Document 3" \
    "../yx -d 3 -f test__documents.yaml" \
    "error"

assert "Invalid Document 0" \
    "../yx -d 0 -f test__documents.yaml" \
    "error"

assert "Invalid Document 'foo'" \
    "../yx -d foo -f test__documents.yaml" \
    "error"

printf "\n=== SCALARS ===\n\n"

assert "Flow Plain" \
    "../yx -f test__scalar-flow-plain.yaml" \
    "succeed" \
    "$(printf "plain\nflow")"

assert "Flow Single Quotes" \
    "../yx -f test__scalar-flow-single.yaml" \
    "succeed" \
    "$(printf "single 'quoted'\nflow")"

assert "Flow Double Quotes" \
    "../yx -f test__scalar-flow-double.yaml" \
    "succeed" \
    "$(printf "double \"quoted\"\nflow")"

assert "Block Folded (clip)" \
    "../yx -f test__scalar-block-folded-clip.yaml" \
    "succeed" \
    "$(printf "block folded\nclip trailing\n")"

assert "Block Literal (clip)" \
    "../yx -f test__scalar-block-literal-clip.yaml" \
    "succeed" \
    "$(printf "block literal\n\nclip trailing\n")"

# TODO: testing blocks with keep/strip trailing newlines -- $(...) appears
#   to lose trailing newlines!  FWIW, yx does appear to output correctly.

printf "\n=== MAPPINGS ===\n\n"

assert "No Key Specified Returns All Keys" \
    "../yx -f test__mappings.yaml" \
    "succeed" \
    "$(printf "foo\nbar\nblah\n")"

assert "Key 'foo' to Scalar" \
    "../yx -f test__mappings.yaml foo" \
    "succeed" \
    "one"

assert "Nested: Three 'blah' Keys to Scalar" \
    "../yx -f test__mappings.yaml blah blah blah" \
    "succeed" \
    "three"

assert "Unknown Key 'wtf'" \
    "../yx -f test__mappings.yaml wtf" \
    "error"

printf "\n=== SEQUENCES ===\n\n"

assert "No Index Returns All Indicies" \
    "../yx -f test__sequences.yaml" \
    "succeed" \
    "$(printf "1\n2\n3\n")"

assert "Invalid Index 0" \
    "../yx -f test__sequences.yaml 0" \
    "error"

assert "Index 1 to Scalar" \
    "../yx -f test__sequences.yaml 1" \
    "succeed" \
    "one"

assert "Nested: Three Indices (3 2 1) to Scalar" \
    "../yx -f test__sequences.yaml 3 2 1" \
    "succeed" \
    "three Two ONE"

assert "Unknown Index 4" \
    "../yx -f test__sequences.yaml 4" \
    "error"

assert "Invalid Index 'foo'" \
    "../yx -f test__sequences.yaml foo" \
    "error"
    
printf "\n=== MAPPING / SEQUENCE COMBINATIONS ===\n\n"

assert "Map -> Sequence -> Map -> Sequence to Scalar" \
    "../yx -f test__map-seq-map-seq.yaml bar 2 argh 2" \
    "succeed" \
    "five"

assert "Sequence -> Map -> Sequence -> Map to Scalar" \
    "../yx -f test__seq-map-seq-map.yaml 2 bar 2 blah" \
    "succeed" \
    "five"

printf "\n*** TESTING RESULTS ***\n"
printf "%d tests, %d failed.\n\n" $TESTS $FAILS
exit $FAILS
